%global gem_name actioncable
%global recompile_js	1

Name:		rubygem-%{gem_name}
Version:	7.0.7
Release:	1
Summary:	WebSocket framework for Rails
License:	MIT
URL:		http://rubyonrails.org
Source0:	https://rubygems.org/gems/%{gem_name}-%{version}.gem
# The gem doesn't ship with the test suite, you may check it out like so
# git clone https://github.com/rails/rails.git
# cd rails/actioncable && git archive -v -o actioncable-7.0.4-tests.txz v7.0.4 test/
Source1: 	%{gem_name}-%{version}-tests.txz
# The source code of pregenerated JS files is not packaged.
# You may get them like so
# git clone https://github.com/rails/rails.git
# cd rails/actioncable && git archive -v -o actioncable-7.0.4-app.txz v7.0.4 app/
Source2: 	%{gem_name}-%{version}-app.txz
# Recompile with script extracted from
# https://github.com/rails/rails/blob/71d406697266fc2525706361b86aeb85183fe4c7/actioncable/Rakefile
Source3: 	recompile_js.rb
# The tools are needed for the test suite, are however unpackaged in gem file.
# You may get them like so
# git clone https://github.com/rails/rails.git --no-checkout
# cd rails && git archive -v -o rails-7.0.4-tools.txz v7.0.4 tools/
Source4: 	rails-%{version}-tools.txz

BuildRequires:	ruby(release)
BuildRequires:	rubygems-devel > 1.3.1
BuildRequires:	ruby >= 2.2.2
BuildRequires:	rubygem(actionpack) = %{version}
BuildRequires:	rubygem(mocha)
BuildRequires:	rubygem(nio4r)
BuildRequires:	rubygem(puma)
BuildRequires:	%{_bindir}/redis-server
BuildRequires:	rubygem(redis)
BuildRequires:	rubygem(hiredis) >= 0.6.3
BuildRequires:	rubygem(websocket-driver)
%if 0%{?recompile_js} > 0
BuildRequires:	rubygem(coffee-script)
BuildRequires:	rubygem(sprockets)
BuildRequires:	nodejs
%endif
BuildArch:	noarch

%description
Structure many real-time application concerns into channels over a single
WebSocket connection.

%package	doc
Summary:	Documentation for %{name}
Requires:	%{name} = %{version}-%{release}
BuildArch:	noarch

%description	doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}%{?prerelease} -b1 -a2 -b4

%build
%if 0%{?recompile_js} > 0
cp -a %{SOURCE3} .
rm -rf lib/assets/compiled
RUBYOPT=-Ilib ruby recompile_js.rb
%endif

gem build ../%{gem_name}-%{version}%{?prerelease}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ln -s %{_builddir}/tools ..
mv %{_builddir}/test .
mv test/client_test.rb{,.disable}
mv test/subscription_adapter/postgresql_test.rb{,.disable}
REDIS_DIR=$(mktemp -d)
redis-server --dir $REDIS_DIR --pidfile $REDIS_DIR/redis.pid --daemonize yes
ruby -Ilib:test -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
kill -INT $(cat $REDIS_DIR/redis.pid)
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}
%{gem_instdir}/app

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md

%changelog
* Thu Aug 17 2023 xu_ping <707078654@qq.com> - 7.0.7-1
- Upgrade to version 7.0.7

* Thu Jan 19 2023 wangkai <wangkai385@h-partners.com> - 7.0.4-1
- Upgrade to version 7.0.4

* Wed May 04 2022 wangkerong <wangkerong@h-partners.com> - 6.1.4.1-1
- Upgrade to 6.1.4.1

* Mon Feb  8 2021 sunguoshuai <sunguoshuai@huawei.com> - 5.2.4.4-1
- Upgrade to 5.2.4.4

* Thu Aug 20 2020 shenleizhao <shenleizhao@huawei.com> - 5.2.3-1
- package init
